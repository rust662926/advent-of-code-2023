use std::io::{BufRead, BufReader, Read};
use std::fs::File;
use std::env;
use color_eyre::eyre::{anyhow, bail, Result, Error};
use std::str::FromStr;
use std::collections::{HashMap, LinkedList};
use std::fmt;

fn all_equal(items: &Vec<impl PartialEq>) -> bool {
    let first = &items[0];
    for item in items[1..].into_iter() {
        if *item != *first {
            return false;
        }
    }
    true
}

struct Map {
    contents: Vec<Vec<bool>>, // true contains star, false is empty
    xmax: usize,
    ymax: usize,
}

impl Map {
    fn get(&self, x: usize, y: usize) -> Result<bool> {
        if x > self.xmax {
            bail!("{x} is larger than the map's width");
        }
        if y > self.ymax {
            bail!("{y} is larger than the map's height")
        }
        Ok(self.contents[y][x])
    }

    fn column_is_empty(&self, col: usize) -> Result<bool> {
        for y in 0..=self.ymax {
            if self.get(col, y)? {
                return Ok(false);
            }
        }
        Ok(true)
    }

    fn expand_column(&mut self, col: usize) {
        self.xmax += 1;
        for  row in &mut self.contents {
            row.insert(col, false);
        }
    }

    fn expand(mut self) -> Result<Self> {
        // do empty columns before empty rows
        for col in 0..=self.xmax {
            if self.column_is_empty(col)? {
                self.expand_column(col)
            }
        }
        // empty rows is simpler
        self.contents = self.contents.into_iter().flat_map(|row| {
            match row.iter().any(|v| *v) {
                true => vec!(row),
                false => vec!(row.clone(), row),
            }
        }).collect::<Vec<_>>();
        self.ymax = self.contents.len();
        Ok(self)
    }

    fn load_from_file(f: &mut BufReader<File>) -> Result<Map> {
        let mut contents: Vec<Vec<bool>> = vec!();
        for line in f.lines() {
            let line = line?;
            contents.push(line.chars().map(|c| c == '#').collect::<Vec<_>>());
        }
        let xmax = contents[0].len() - 1;
        let ymax = contents.len() - 1;
        Ok(Map{contents, xmax, ymax})
    }
}

impl fmt::Display for Map {
    fn fmt(&self, f: &mut fmt::Formatter<'_>) -> fmt::Result {
        for line in self.contents.iter() {
            write!(f, "{}\n",
                line.iter().map(|v| {
                    match v {
                        true => '#',
                        false => '.',
                    }
                }).collect::<String>()
            )?;
        }
        Ok(())
    }
}

fn main() -> Result<()> {
    color_eyre::install()?;
    let f = File::open(env::args().nth(1).ok_or_else(|| anyhow!("No filename specified"))?)?;
    let mut f = BufReader::new(f);

    let mut map = Map::load_from_file(&mut f)?;
    println!("Map\n{map}");
    map = map.expand()?;
    println!("Expanded Map\n{map}");

    Ok(())
}
